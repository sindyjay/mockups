const express = require('express')
const PORT = 5000
const app = express()

// router example
app.use(express.Router().post(
    '/api/rest/human/friendly/path',
    async (req, res) => {
        const {login, password} = req.body;
        return res.status(200).json({
            login: login,
            password: password,
            body: req.body
        })
    }
))

// start application:
app.listen(PORT, () => console.log(`Server has been started on port ${PORT}...`));
